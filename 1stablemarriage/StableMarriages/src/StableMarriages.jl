module StableMarriages

export stablemarriages

mutable struct Man
    i::Int
    prefs::Array{Int}
    proposeto::Int
end
mutable struct Woman
    invprefs::Array{Int}
    engagedto::Int
end

prefer(♀::Woman, ♂i::Int) = ♀.invprefs[♀.engagedto] > ♀.invprefs[♂i]

function invpref(v)
    length(v) < 100000 && return invperm(v)
    res = similar(v)
    @inbounds Threads.@threads for i=1:length(v)
        res[v[i]] = i
    end
    res
end

## 537 ms
function converttoints(lines::Array{String,1})
    intlists = map(lines) do line
        Threads.@spawn map(split(line, " ", keepempty=false)) do num
            parse(Int, num)
        end
    end
    vcat(map(fetch, intlists)...)
end

## 324 ms
function readints(inp)
    input = IOBuffer(read(inp))
    numbers = Array{Int,1}(undef, input.size ÷ 2)
    i = 1
    while !eof(input)
        x = read(input, UInt8)
        acc = 0
        while !isspace(Char(x))
            acc *= 10
            acc += x-0x30
            x = read(input, UInt8)
        end
        numbers[i] = acc
        i += 1
    end
    numbers[1:i-1]
end

function parseints(v)
    input = IOBuffer(v)
    numbers = Array{Int,1}(undef, input.size ÷ 2 + 1)
    i = 1
    while !eof(input)
        x = read(input, UInt8)
        acc = 0
        while !isspace(Char(x))
            acc *= 10
            acc += x-0x30
            x = read(input, UInt8)
        end
        numbers[i] = acc
        i += 1
    end
    numbers[1:i-1]
end

## Only handles one space char in a row
# function parseintsptr(v)
#     len = length(v)
#     numbers = Array{Int,1}(undef, len ÷ 2 + 1)
#     n = 1
#     acc = 0
#     numptr = pointer(numbers)
#     vptr = pointer(v)
#     for i=1:len
#         x = unsafe_load(vptr,i)
#         if isspace(Char(x))
#             acc = 0
#             n += 1
#             continue
#         end
#         acc *= 10
#         acc += x-0x30
#         unsafe_store!(numptr, acc, n)
#     end
#     numbers[1:n-1]
# end
function parseintsptr(v)
    len = length(v)
    numbers = Array{Int,1}(undef, len ÷ 2 + 1)
    numptr = pointer(numbers)
    vptr = pointer(v)
    n = 1
    i = 1
    while i <= len
        x = unsafe_load(vptr,i)
        i += 1
        acc = 0
        while !isspace(Char(x))
            acc *= 10
            acc += x-0x30
            x = unsafe_load(vptr,i)
            i += 1
        end
        unsafe_store!(numptr, acc, n)
        n += 1
    end
    numbers[1:n-1]
end

## 144 ms
function readintsparallel(inp)
    data = read(inp)
    N = Threads.nthreads()
    chunks = Array{IOBuffer,1}(undef, N)
    len = length(data)
    chunksize = len ÷ N
    tasklist = (Task)[]
    laststop = 0
    for i=1:N
        start = laststop + 1
        laststop = i==N ? len : chunksize * i
        while laststop != len && !isspace(Char(data[laststop]))
            laststop += 1
        end
        stop = laststop
        push!(tasklist, Threads.@spawn parseints(@view data[start:stop]))
    end
    vcat(map(fetch, tasklist)...)
end

## 131 ms
function readintsparallelptr(inp)
    data = read(inp)
    N = Threads.nthreads()
    chunks = Array{IOBuffer,1}(undef, N)
    len = length(data)
    chunksize = len ÷ N
    tasklist = (Task)[]
    laststop = 0
    for i=1:N
        start = laststop + 1
        laststop = i==N ? len : chunksize * i
        while laststop != len && !isspace(Char(data[laststop]))
            laststop += 1
        end
        stop = laststop
        push!(tasklist, Threads.@spawn parseintsptr(@view data[start:stop]))
    end
    vcat(map(fetch, tasklist)...)
end


function getinitialpersonlists(fulllist::Array{Int,1})
    N = fulllist[1]
    preflist = @view fulllist[2:end]
    women = Array{Woman,1}(undef, N)
    men = Array{Man,1}(undef, N)
    @inbounds for i=1:N*2
        s = 1+(i-1)*(N+1)
        prefs = @view preflist[s:s+N]
        index = prefs[1]
        if isassigned(women,index)
            men[index] = Man(index, prefs[2:end], 0)
        else
            women[index] = Woman(invpref(@view prefs[2:end]), 0)
        end
    end
    women, men
end

## Test 4   w/warmup
## 1.52 s   0.82 s  converttoints
## 0.95 s   0.48 s  readints
## 0.82 s   0.35 s  readintsparallel
## 0.80 s   0.31 s  readintsparallelptr
function stablemarriages(input)
    preflist = readintsparallelptr(input)
    #preflist = readintsparallel(input)
    #preflist = readints(input)
    #preflist = converttoints(readlines(input))
    women, men = getinitialpersonlists(preflist)
    unengagedmen = Set{Man}(men)
    @inbounds while !isempty(unengagedmen)
        for man in unengagedmen
            man.proposeto += 1
            target♀ = women[man.prefs[man.proposeto]]
            if target♀.engagedto == 0 || prefer(target♀, man.i)
                if target♀.engagedto != 0
                    rejected = men[target♀.engagedto]
                    push!(unengagedmen, rejected)
                end
                pop!(unengagedmen, man)
                target♀.engagedto = man.i
            end
        end
    end
    map(women) do ♀
        string(♀.engagedto, "\n")
    end |> join |> print
end

main() = stablemarriages(Base.stdin)

Base.@ccallable function julia_main()::Cint
    try
        main()
    catch
        Base.invokelatest(Base.display_error, Base.catch_stack())
        return 1
    end
    return 0
end

if abspath(PROGRAM_FILE) == @__FILE__
    main()
end

end # module
