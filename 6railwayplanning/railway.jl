struct FlowEdge
    src::Int
    dst::Int
    flow::Int
    cap::Int
end
FlowEdge(src::Int, dst::Int, cap::Int) = FlowEdge(src, dst, 0, cap)
niledge() = FlowEdge(0,0,0)

struct FlowGraph
    nv::Int
    adj::Array{Array{FlowEdge,1},1}
end

function FlowGraph(sources::Array{Int,1}, destinations::Array{Int,1}, capacities::Array{Int,1})
    N = length(Set(sources) ∪ Set(destinations))
    M = length(capacities)

    graph = FlowGraph(
        N,
        fill([],N)
    )

    for (src,dst,cap) in zip(sources,destinations,capacities)
        edge1 = FlowEdge(src,dst,cap)
        edge2 = FlowEdge(dst,src,cap)
        push!(graph.adj[src],edge1)
        push!(graph.adj[dst],edge2)
    end

    graph
end

function update!(g::FlowGraph,e::FlowEdge)
    i = findfirst(x->dst(x)==dst(e),g.adj[src(e)])
    g.adj[src(e)][i] = e
end

function remove!(g::FlowGraph,e::FlowEdge)
    i1 = findfirst(x->dst(x)==dst(e),g.adj[src(e)])
    i2 = findfirst(x->dst(x)==src(e),g.adj[dst(e)])
    deleteat!(g.adj[src(e)],i1)
    deleteat!(g.adj[dst(e)],i2)
end

src(e::FlowEdge) = e.src
dst(e::FlowEdge) = e.dst
flow(e::FlowEdge) = e.flow
cap(e::FlowEdge) = e.cap
nil(e::FlowEdge) = e.src === 0
nv(g::FlowGraph) = g.nv
Base.show(io::IO, g::FlowGraph) = print(io, g.adj)

function bfs(graph::FlowGraph, pred::Array{FlowEdge,1}, q::Array{Int},
             s::Int, t::Int)
    for cur in q
        adjacent = graph.adj[cur]
        for e in adjacent
            if nil(pred[dst(e)]) && dst(e) ≠ s && cap(e) > flow(e)
                pred[dst(e)] = e
                push!(q, dst(e))
            end
        end
    end
end

function bfs(graph::FlowGraph, s::Int, t::Int)
    pred = fill(niledge(), nv(graph))
    q = [s]
    bfs(graph, pred, q, s, t)
    pred
end

function edmondskarp(G::FlowGraph, s::Int, t::Int)
    pred = bfs(G,s,t)

    accflow = 0
    while !nil(pred[t])
        Δ = typemax(Int)

        #Find minimum flow
        e = pred[t]
        while !nil(e)
            Δ = min(Δ, cap(e) - flow(e))
            e = pred[src(e)]
        end

        #Update edges
        e = pred[t]
        while !nil(e)
            #rev_e = G.adjmat[dst(e),src(e)]
            i = findfirst(x->dst(x)==src(e),G.adj[dst(e)])
            rev_e = G.adj[dst(e)][i]
            new_e = FlowEdge(src(e), dst(e), flow(e) + Δ, cap(e))
            new_rev_e = FlowEdge(dst(e),src(e), flow(rev_e) - Δ, cap(e))

            update!(G, new_e)
            update!(G, new_rev_e)
            e = pred[src(e)]
        end

        accflow += Δ
        pred = bfs(G,s,t)
    end
    accflow
end

function railway(input)
    lines = readlines(input)
    nums = parse.(Int,split(lines[1]," ",keepempty=false))

    N = nums[1]
    M = nums[2]
    C = nums[3]
    P = nums[4]

    #Structure - Undirected graph
    sources = Array{Int,1}(undef,M)
    destinations = Array{Int,1}(undef,M)
    capacities = Array{Int,1}(undef,M)

    #M edges
    for (i,line) in enumerate(lines[2:M+1])
        nums = parse.(Int,split(line, " ", keepempty=false))
        sources[i] = nums[1] + 1
        destinations[i] = nums[2] + 1
        capacities[i] = nums[3]
    end

    graph = FlowGraph(sources, destinations, capacities)

    #Number of routes to remove
    toremove = Array{Int,1}(undef,P)
    for (i,line) in enumerate(lines[M+2:end])
        toremove[i] = parse(Int,line) + 1
    end

    #Calculate the amount of removable edges
    #Through binary search on the removable edges
    cachedC = fill(typemax(Int), length(toremove))
    (maxIdx, maximumFlow) = binary_search(graph, C, N, sources, destinations, capacities, toremove, cachedC, 1, length(toremove))

    println(maxIdx, " ", maximumFlow)
end

function binary_search(G, C, N, sources, destinations, capacities, toremove, cachedC, lower, upper)
    idx = (upper + lower) ÷ 2

    #Copy graph
    graph = deepcopy(G)

    #Remove edges from graph
    for i=1:idx
        e_idx = toremove[i]
        remove!(graph, FlowEdge(sources[e_idx], destinations[e_idx], capacities[e_idx]))
    end

    #Spawn thread and calculate path
    graph2 = deepcopy(graph)
    tmpc1 = Threads.@spawn cachedC[idx] == typemax(Int) ? edmondskarp(graph, 1, N) : cachedC[idx]

    e_idx = toremove[idx+1]
    remove!(graph2, FlowEdge(sources[e_idx], destinations[e_idx], capacities[e_idx]))
    cachedC[idx+1] == typemax(Int) && (cachedC[idx+1] = edmondskarp(graph2, 1, N))

    #Wait for thread
    cachedC[idx] = fetch(tmpc1)

    #Base case
    if cachedC[idx] >= C && cachedC[idx+1] < C
        return (idx, cachedC[idx])
    end

    if (cachedC[idx] < C)
        upper = idx
        return binary_search(G, C, N, sources, destinations, capacities, toremove, cachedC, lower, upper)
    else
        lower = idx
        return binary_search(G, C, N, sources, destinations, capacities, toremove, cachedC, lower, upper)
    end
end

main() = railway(Base.stdin)

main()


# First version of calculating amount of removable edges:
# With sequential calculating amount of removable
    # #Calculate the amount of removable edges
    # maximumFlow = 0
    # for (i, e_idx) in enumerate(toremove)
    #     #Remove edge from graph
    #     del_e = FlowEdge(sources[e_idx], destinations[e_idx], capacities[e_idx])
    #     remove!(graph, del_e)

    #     #Calculate flow
    #     flow = edmondskarp(deepcopy(graph), 1, N)

    #     if (flow < C)
    #         println(i-1, " ", maximumFlow)
    #         return
    #     end

    #     maximumFlow = flow
    # end

# Second version of calculating amount of removable edges:
# with binary search in a while loop
    # while !(cachedC[idx] >= C && cachedC[idx+1] < C) #|| cachedC[idx] == typemax(Int)
    #     #Copy graph
    #     gtmp = deepcopy(graph)

    #     #Remove edges from graph
    #     for i=1:idx
    #         e_idx = toremove[i]
    #         remove!(gtmp, FlowEdge(sources[e_idx], destinations[e_idx], capacities[e_idx]))
    #     end

    #     cachedC[idx] == typemax(Int) && (cachedC[idx] = edmondskarp(deepcopy(gtmp), 1, N))

    #     e_idx = toremove[idx+1]
    #     remove!(gtmp, FlowEdge(sources[e_idx], destinations[e_idx], capacities[e_idx]))

    #     cachedC[idx+1] == typemax(Int) && (cachedC[idx+1] = edmondskarp(gtmp, 1, N))

    #     maximumFlow = cachedC[idx]
    #     maxIdx = idx

    #     if (cachedC[idx] < C)
    #         idx = max(1, idx ÷ 2)
    #     else
    #     # elseif (cachedC[idx] > C)
    #         idx = (idx + length(toremove)) ÷ 2
    #         # idx += (length(toremove) - idx) ÷ 2
    #     end
    # end

#Ford-Fulkerson
#   1) Start with a flow f(e) = 0 for every e ∈ E
#   2) Look for a simple path p from s to t such that on every edge (u, v) in p we can increase the flow in the direction from u to v
#   3) If we could not find any such path, we have found the maximum flow and terminate the algorithm
#   4) Let edge e = (u, v) on p have a value delta(e), which means room for improvement, or how we can increase the flow on that edge
#   5) Let δ be the minimum of all delta(e) on p
#   6) Increase the flow by δ along the path p
#   7) Goto 2

#Ford-Fulkerson
# procedure ford_fulkerson(G,s,t,c)
# for each e € E do f(e) <-- 0
# Gf <- create initial residual graph
# while (p <- find_path(Gf)) != nil do
#   augment G using p
#   update f in G
#   update cf in Gf