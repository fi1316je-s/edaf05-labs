import Printf.@printf

#closest(P_X, P_y, n)
# Divide P_x in two two arrays L_x and R_x (left and right)
# Divide P_y in two two arrays L_y and R_y (left and right)
# Solve the subproblems (L_x, L_y, n/2) and (R_x, R_y, n/2)
# Compute delta as he minimum from these subproblems
# Create the set S_y from P_y
# Check each point in S_y to see if any nearby point is closer than delta
#end

distance(a::Tuple{Number,Number},b::Tuple{Number,Number}) = √((Int(a[1]) - Int(b[1]))^2 + (Int(a[2]) - Int(b[2]))^2) 

function closest(Pₓ, Pᵧ, n)
    if n == 3
        return minimum([distance(Pₓ[1],Pₓ[2]), distance(Pₓ[1],Pₓ[3]), distance(Pₓ[2],Pₓ[3])])
    elseif n == 2
        return distance(Pₓ[1],Pₓ[2])
    end

    midₓ = (1 + length(Pₓ)) ÷ 2
    
    Lₓ = @view Pₓ[1 : midₓ]
    Rₓ = @view Pₓ[midₓ + 1 : end]
    
    Lᵧ = filter(x-> x <= midₓ, Pᵧ)
    Rᵧ = filter(x-> x > midₓ, Pᵧ) .- midₓ

    δₗ = closest(Lₓ, Lᵧ, n ÷ 2)
    δᵣ = closest(Rₓ, Rᵧ, n ÷ 2)
    δ = min(δₗ, δᵣ)
    
    xₚ = Pₓ[midₓ]

    minᵧ = @inbounds searchsortedfirst(Pₓ[Pᵧ], xₚ[1] - δ, by=a->a[1])
    maxᵧ = @inbounds searchsortedlast(Pₓ[Pᵧ], xₚ[1] + δ, by=a->a[1])
    Sᵧ = @view Pᵧ[minᵧ : maxᵧ]

    C = 16
    @inbounds for i=1:length(Sᵧ)
        for j=i+1:C+i
            if j <= length(Sᵧ)
                δ = min(δ, distance(Pₓ[Sᵧ[i]],Pₓ[Sᵧ[j]]))
            end
        end
    end
    δ
end

function closestpair(input)
    lines = readlines(input)
    nums = parse(Int,lines[1])
    N = nums[1]

    Pₓ = Array{Tuple{Int32,Int32},1}(undef,N)
    for (i,line) in enumerate(lines[2:end])
        nums = map(x->parse(Int,x),split(line, " ", keepempty=false))   
        Pₓ[i] = (nums[1],nums[2])
    end

    sort!(Pₓ, by=a->a[1])
    Pᵧ = sortperm(Pₓ, by=a->a[2])

    δ = closest(Pₓ, Pᵧ, N)
    @printf "%.6f\n" δ
end

main() = closestpair(Base.stdin)

main()
