using LightGraphs, SimpleWeightedGraphs, DataStructures
import Base: isless

Base.isless(e1::SimpleWeightedEdge,e2::SimpleWeightedEdge) = isless(weight(e1),weight(e2))

function makegraph(input)
    lines = readlines(input)
    nums = map(x->parse(Int,x),split(lines[1], " ", keepempty=false))
    N = nums[1]
    M = nums[2]
    
    sources = Array{Int,1}(undef,M)
    destinations = Array{Int,1}(undef,M)
    weights = Array{Int,1}(undef,M)
    
    # Read remaining lines, i.e. M edge
    for (i,line) in enumerate(lines[2:end])
        nums = map(x->parse(Int,x),split(line, " ", keepempty=false))
        sources[i] = nums[1]
        destinations[i] = nums[2]
        weights[i] = nums[3]
    end
    
    SimpleWeightedGraph(sources, destinations, weights)
end

# Pseudocode for Kruskals algorithm
# T <- empty set
# B <- Edges from graph (E)
# while B is non empty
#    Select an edge e with minimal weight
#    If T union {e} does not create a cycle
#       Add e to T
#    Remove e from B
# return T
function kruskals(graph)
    wsum = 0
    es = SortedSet(edges(graph))
    n = nv(graph)
    parents = fill(0, n)
    sizes = fill(0, n)
    while !isempty(es)
        e = pop!(es)
        if notcycle!(src(e),dst(e),parents)
            mst_union!(src(e),dst(e),parents,sizes)
            wsum += weight(e)
        end
    end
    wsum
end

notcycle!(u,v,parents) = find!(u,parents) != find!(v,parents)
    
# Pseudocode for find
# p ← v
# while parent(p) ≠ null do
#     p ← parent(p)
# while parent(v) ≠ null do
#     w ← parent(v)
#     parent(v) ← p
#     v ← wait
# return p
function find!(v, parents)
    p = v
    while parents[p] != 0
        p = parents[p]
    end
    while parents[v] != 0
        w = parents[v]
        parents[v] = p
        v = w
    end
    p
end

# Pseudocode for union
# u ← find(u)
# v ← find(v)
# if size(u) < size(v) then
#     parent(u) ← v
#     size(v) ← size(u) + size(v)
# else
#     parent(v) ← u
#     size(u) ← size(u) + size(v)
function mst_union!(u,v,parents,sizes)
    u = find!(u, parents)
    v = find!(v, parents)
    if sizes[u] < sizes[v]
        parents[u] = v
        sizes[v] = sizes[u] + sizes[v]
    else
        parents[v] = u
        sizes[u] = sizes[u] + sizes[v]
    end
end

function makingfriends(input)
    graph = makegraph(input)
    wsum = kruskals(graph)
    println(wsum)   
end

main() = makingfriends(Base.stdin)

#main() = println(sum(weight.(kruskal_mst(makegraph(Base.stdin)))))

main()
