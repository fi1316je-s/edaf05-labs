const δ = -4
const MISSING = '*'

mutable struct Cost
    v::Int
end

@inbounds function opt(x, y, i, j, lookup, α, A)
    if isassigned(A,i,j) return A[i,j].v end
    j == 1 && (return (A[i,j] = Cost((i - 1) * δ)).v)
    i == 1 && (return (A[i,j] = Cost((j - 1) * δ)).v)
    A[i,j] = maximum([
        α[lookup[x[i - 1]], lookup[y[j - 1]]] + opt(x, y, i - 1, j - 1, lookup, α, A),
        δ + opt(x, y, i, j - 1, lookup, α, A),
        δ + opt(x, y, i - 1, j, lookup, α, A),
    ]) |> Cost
    A[i,j].v
end

function align_rna(line, lookup, cost)
    #Initialization:
    query = split(line, " ", keepempty=false)
    m = length(query[1])
    n = length(query[2])
    maxlength = m+n+1
    i = m + 1
    j = n + 1
    
    #OPTION 1: Calculate matrix with opt: 
    # dp = Array{Cost,2}(undef, length(query[1])+1, length(query[2])+1)
    # opt(query[1], query[2], length(query[1])+1, length(query[2])+1, lookup, cost, dp)
    # dp = map(x->x.v,dp)

    #OPTION 2: Calculate matrix with loops:
    dp = zeros(Int, m+1, n+1)
    @inbounds for i=1:max(i,j)
        i <= m+1 && (dp[i,1] = (i - 1) * δ)
        i <= n+1 && (dp[1,i] = (i - 1) * δ)
    end

    query1 = map(x->lookup[x], collect(query[1]))
    query2 = map(x->lookup[x], collect(query[2]))

    @inbounds for j=2:n+1, i=2:m+1
        dp[i,j] = max(
            dp[i-1,j-1] + cost[query1[i-1],query2[j-1]], max(
            dp[i-1,j] + δ,
            dp[i,j-1] + δ)
        )
    end
    
    #Compute new strings: 
    buf1 = IOBuffer()
    buf2 = IOBuffer()
    while !(i == 1 || j == 1)
        if dp[i - 1,j - 1] + cost[lookup[query[1][i - 1]],lookup[query[2][j - 1]]] == dp[i,j]
            write(buf1, query[1][i - 1])
            write(buf2, query[2][j - 1])
            i-=1
            j-=1
        elseif dp[i - 1,j] + δ == dp[i,j]
            write(buf1, query[1][i - 1])
            write(buf2, MISSING)
            i-=1
        elseif dp[i,j - 1] + δ == dp[i,j]
            write(buf1, MISSING)
            write(buf2, query[2][j - 1])
            j-=1
        end
    end

    #Write remaining characters:
    while buf1.size <= maxlength
        if i > 1
            i-=1
            write(buf1, query[1][i])
        else
            write(buf1, MISSING)
        end
    end
    while buf2.size <= maxlength
        if j > 1
            j-=1
            write(buf2, query[2][j])
        else
            write(buf2, MISSING)
        end
    end

    #Print strings: 
    ans1 = String(take!(buf1))
    ans2 = String(take!(buf2))
    for i=1:(m + n)
        if ans1[i] === MISSING && ans2[i] === MISSING
            return string(reverse(ans1[1:i - 1]), " ", reverse(ans2[1:i - 1]))
        end
    end
end

function gorilla(input)
    lines = readlines(input)
    lookup = Dict(map(p -> (p[2][begin],p[1]), enumerate(split(lines[1]," ", keepempty=false))))
    N = length(lookup)
    cost = hcat(map(line->map(x->parse(Int,x),split(line, " ", keepempty=false)),lines[2:N+1])...)

    tasks = Task[]
    # Read all queries
    for line in lines[N+3:end]
        push!(tasks, Threads.@spawn align_rna(line, lookup, cost))
    end
    println.(fetch.(tasks))
end

main() = gorilla(Base.stdin)

main()
