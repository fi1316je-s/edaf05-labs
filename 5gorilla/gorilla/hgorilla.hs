import qualified Data.Map.Strict as Map
import qualified Data.Matrix as Matrix
import Data.Map.Strict (Map)
import Data.Matrix

main :: IO ()
main = do
    input <- getContents
    let (letters:rest) = lines input
        lookup = Map.fromList $ zip (map head . words $ letters) [1..]
        n = length lookup
        cost = makeCostMatrix $ take n rest
        query1 = rest !! (n + 1)

    putStr query1


makeCostMatrix :: [String] -> Matrix Int
makeCostMatrix rows = Matrix.fromLists $ map (map read . words) rows

opt :: String -> String -> Int -> Int -> Map Char Int -> Matrix Int -> Matrix Int -> Int
opt x y i j lookup cost a = 1
opt _ _ i 0 _ _ a = (newValue, Matrix.setElem newValue (i,0) a)
    where newValue = i*delta